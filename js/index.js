$(function () {
    $("[data-toogle='tooltip']").tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 3000
    });
    var id;
    $('#reserveModal').on('show.bs.modal', function (e) {
        id = e.relatedTarget.attributes.id.value;
        $(id).prop('disabled', true);
        $(id).removeClass('btn-secondary')
        $(id).addClass('btn-dark')
        console.log("El modal se esta mostrando");
    })

    $('#reserveModal').on('shown.bs.modal', function () {
        console.log('El modal se mostró');
    })

    $('#reserveModal').on('hide.bs.modal', function () {
        console.log('El modal se esta ocultando');
    })

    $('#reserveModal').on('hidden.bs.modal', function () {
        $(id).prop('disabled', false);
        $(id).removeClass('btn-dark')
        $(id).addClass('btn-secondary')
        console.log('El modal se ocultó');
    })
})